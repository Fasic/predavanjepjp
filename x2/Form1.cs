﻿/*
2 FORME

1. Forma:
2 TextBoxa, broj1 i broj 2
i dugme koje otvara formu 2

2. Forma:
4 radio dugmeta (+ - * /)
dugme i textBox, kada se stisne dugme u txt boxs se upisuje resenje b1 + b2 

 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace x2
{
	/// <summary>
	/// Description of Form1.
	/// </summary>
	public partial class Form1 : Form
	{
		public String x;
		double b1, b2;
		
		public Form1(double b1, double b2)
		{
			InitializeComponent();
			this.b1 = b1;
			this.b2 = b2;
		}
		void Button1Click(object sender, EventArgs e)
		{	
			if(radioButton1.Checked) x = (b1 + b2) + "";
			else if(radioButton2.Checked) x = (b1 - b2) + "";
			else if(radioButton3.Checked) x = (b1 / b2) + "";
			else if(radioButton4.Checked) x = (b1 * b2) + "";
			else MessageBox.Show("Izabri neku od opcija!");
			
			if(x != "" && x != null) this.Close();
			
		}
	}
}
