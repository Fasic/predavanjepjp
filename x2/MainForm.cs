﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace x2
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
		}
		void Label1Click(object sender, EventArgs e)
		{

		}
		void Button1Click(object sender, EventArgs e)
		{
			double b1, b2;
			bool dal1 = double.TryParse(textBox1.Text, out b1);
			bool dal2 = double.TryParse(textBox2.Text, out b2);
			if(dal1 && dal2){
				Form1 settingsForm = new Form1(b1, b2);
				settingsForm.ShowDialog();
				textBox3.Text = settingsForm.x;
			} else{
				if(!dal1)  MessageBox.Show("Molim vas da unesete broj 1 u odgovarajucem formatu!");
				if(!dal2)  MessageBox.Show("Molim vas da unesete broj 2 u odgovarajucem formatu!");
			}
			
		}
	}
}
